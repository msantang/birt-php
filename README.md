# Birt reporting for PHP #

Birt reporting library for php using Java Bridge

### General ###

* Integration with birt ussing open source Java Bridge [java bridge](http://php-java-bridge.sourceforge.net/pjb/)
* JB could be on the same or a remote server.
* Last version of birt 4.5
* Easy to use

### Install ###
First you need java bridge runnig (local o remote)

[Here](https://bitbucket.org/msantang/javabridge) you can get java bridge with birt 4.5 ready to be droped on jetty or tomcat ([https://bitbucket.org/msantang/javabridge](https://bitbucket.org/msantang/javabridge))

Add to composer.json:

    "msantang/birt-php"

Update packages

    composer update

### Getting Started ###

```
#!php
<?php
use Msantang\BirtPhp\Engine;
use Msantang\BirtPhp\Design;

$e = new Engine;

// Open local file into a bytearray (support local or remote jb)
$d = Design::fromLocalFile(base_path().'/test.rptdesign');

// or open a file on Jb file system
$d = Design::fromFile('/some/folder/test.rptdesign');


$e->open($d);
  ->generateTask()
  ->setFormat('html')
  ->setParameter('sample','Hello', 'string')
  ->render()
  ->sendStream(); // Send report with format headers
  //->getStream() get the report as a string
```

### Get Report Parameters ###
Get all report parameters
```
#!php
<?php
use Msantang\BirtPhp\Engine;
use Msantang\BirtPhp\Design;

$e = new Engine;

// Open local file into a bytearray (support local or remote jb)
$d = Design::fromLocalFile(base_path().'/test.rptdesign');
$e->open($d);
$p = $e->getReportParams();
```

Format:
```
   [
       ['TypeName']          
       ['Name']              
       ['DataType']          
       ['SelectionListType']
       ['ControlType']       
       ['PromptText']        
       ['isRequired']        
       ['isHidden']          
       ['DefaultValue']
       ['Options']         // combo options
   ]
```