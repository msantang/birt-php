<?php namespace Msantang\BirtPhp;


/**
 * Msantang\Birt\Design
 *
 * @author Martin Alejandro Santangelo
 */
interface Preprocessor
{
	public function run($xml);
}
