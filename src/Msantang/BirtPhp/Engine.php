<?php namespace Msantang\BirtPhp;


/**
 * Msantang\Birt\Birt
 *
 * This class generate reports with Birt and Javabrdige (open source)
 *
 * @author Martin Alejandro Santangelo
 */
if (!defined('JAVA_BRIDGE_PATH')) {
    define ('JAVA_BRIDGE_PATH' , 'http://127.0.0.1:8080/JavaBridge');
}

require_once(JAVA_BRIDGE_PATH."/java/Java.inc");

use java;

/**
 * Report Engine
 *
 * @author Martin Alejandro Santangelo
 */
class Engine
{
    const TYPE_TIME	        = 8;
    const TYPE_DATE	        = 7;
    const TYPE_INTEGER   	= 6;
    const TYPE_BOOLEAN  	= 5;
    const TYPE_DATE_TIME	= 4;
    const TYPE_DECIMAL	    = 3;
    const TYPE_FLOAT	    = 2;
    const TYPE_STRING	    = 1;
    const TYPE_ANY      	= 0;


    const CTRL_TEXT_BOX	     = 0;
    const CTRL_LIST_BOX	     = 1;
    const CTRL_RADIO_BUTTON  = 2;



    private $_birtEngine   = null;
    private $_outputStream = null;
    private $design        = null;
    private $_params       = array();
    private $_task         = null;

    private $baseImageURL   = '';
    private $imageDirectory = '';

    private $taskGetParameters;


    public function __construct()
    {
        $this->_createEngine();
    }

    /**
     * Clear report's parameters
     */
    public function clearParams()
    {
        $this->_params = array();
    }

    /**
     * @param $b string Base URL path for images (html)
     */
    public function setBaseImageURL($b)
    {
        $this->baseImageURL = $b;
    }

    /**
     * @param $d string Images file system folder
     */
    public function setImageDirectory($d)
    {
        $this->imageDirectory = $d;
    }

    /**
     * generate render options
     */
    protected function getOutputFormat($format)
    {
        $fmt = null;
        switch ($format) {
            case "pdf":
                $fmt = new java("org.eclipse.birt.report.engine.api.PDFRenderOption");

                $fmt->setOutputFormat("pdf");
                break;
            case "html":
                $fmt = new java("org.eclipse.birt.report.engine.api.HTMLRenderOption");

                $ih = new java("org.eclipse.birt.report.engine.api.HTMLServerImageHandler");

                $fmt->setImageHandler($ih);
                $fmt->setBaseImageURL($this->baseImageURL);
				$fmt->setImageDirectory($this->imageDirectory);
				$fmt->setEnableAgentStyleEngine(true);
                $fmt->setOutputFormat("html");
                break;
            case "msword":
                $fmt = new java("org.eclipse.birt.report.engine.api.RenderOption");
                $fmt->setOutputFormat("doc");
                break;
            case "xls":
                $fmt = new java("org.eclipse.birt.report.engine.api.RenderOption");
                $fmt->setOutputFormat("xls");
                break;
            default:
                throw new Exception("Formato '$format' desconocido");
        }
        return $fmt;
    }

    /**
     * Setea parametros al reporte
     *
     * @param string $param Nombre del parametro
     * @param mixed  $value Valor del parametro
     * @param string $type  Tipo de dato int|string|datetime
     * @throws Exception
     * @returns Engine
     */
    public function setParameter($param, $value, $type = 'int')
    {
        $type = strtolower($type);

        switch ($type) {
            case 'int':
                $this->_params[$param] = new Java("java.lang.Integer", $value);
                break;
            case 'string':
                $this->_params[$param] = new Java("java.lang.String", $value);
                break;
            case 'datetime':
                // el Date de Java va en milisegundos, el timestamp de PHP en segundos
                $this->_params[$param] = new Java("java.util.Date", strtotime($value)*1000);
                break;
            default:
                throw new Exception("Tipo $type no soportado");
        }
        return $this;
    }

    /**
     * Instancia el engine del birt
     *
     * @returns Engine
     */
    private function _createEngine()
    {
		$ctx = java_context()->getServletContext();
		$this->_birtEngine = java("org.eclipse.birt.php.birtengine.BirtEngine")->getBirtEngine($ctx);
		java_context()->onShutdown(java("org.eclipse.birt.php.birtengine.BirtEngine")->getShutdownHook());
		$this->_birtEngine->getConfig()->setResourcePath(APPLICATION_PATH."/../birt/Reports");

        return $this;
    }

    /**
     * Render loaded report
     *
     * @throws Exception
     * @return Engine
     */
    public function render()
    {
        if (!$this->_task) throw new Exception('No task created. First call generateTask');

        foreach ($this->_params as $param => $value) {
            $this->_task->setParameterValue($param, $value);
        }

        $this->_outputStream = new java("java.io.ByteArrayOutputStream");

        $this->_task->getRenderOption()->setOutputStream($this->_outputStream);

        $this->_task->run();

        return $this;
    }

    public function __destruct()
    {
        if($this->_task) $this->_task->close();
    }

    /**
     * @param $format
     * @return $this
     * @throws Exception
     */
    public function setFormat($format)
    {
        $fmt = $this->getOutputFormat($format);
        $fmt->setOutputFormat($format);
        $this->_task->setRenderOption($fmt);

        return $this;
    }

    /**
     * @param string $locale
     * @return $this
     */
    public function generateTask( $locale = "es_AR")
    {
        if ($this->_task) $this->_task->close();

        $this->_task = $this->_birtEngine->createRunAndRenderTask($this->design);
        $lc = new java("java.util.Locale", $locale);
        $this->_task->setLocale($lc);

        return $this;
    }

    /**
     * @param Design $design
     * @return $this
     */
    public function open(Design $design)
    {
        $this->design = $this->_birtEngine->openReportDesign($design->getDesign());
        return $this;
    }
/*
    public function getPageCount()
    {
        return $this->design->getPageCount();
    }*/

    /**
     * Return given parameters
     */
    public function getParams()
    {
        return $this->_params;
    }

    /**
     * Obtiene el reporte renderizado
     */
    public function getStream()
    {
    	return java_values($this->_outputStream->toByteArray());
    }


    public function getEngine()
    {
        return $this->_birtEngine;
    }

    /**
     * @return null
     */
    public function getDesign()
    {
        return $this->design;
    }



    /**
     * Returns the number of pages in the report
     *
     * @return int
     */
    function getNumberOfPages()
    {
        return $this->_birtEngine->getReportHTMLPageCount();
    }

    public function getTask()
    {
        return $this->_task;
    }

    public function setPageNumber($p)
    {
        $this->_task->setPageNumber($p);
    }

    /**
     * @param $r rango por ejemplo "1-5"
     */
    public function setPageRange($r)
    {
        $this->_task->setPageRange($r);
    }

    public function getReportParams()
    {
        $p = $this->getTaskGetParameters()->getParameterDefns( true );
        $rt = [];
        foreach($p as $v) {
            $tmp = [];
            $tmp['TypeName']          = java_values($v->getTypeName());
            $tmp['Name']              = java_values($v->getName());
            $tmp['DataType']          = java_values($v->getDataType());
            $tmp['SelectionListType'] = java_values($v->getSelectionListType());
            $tmp['HelpText']          = java_values($v->getHelpText());
            $tmp['ControlType']       = java_values($v->getControlType());
            $tmp['PromptText']        = java_values($v->getPromptText());
            $tmp['isRequired']        = (java_values($v->isRequired()))?1:0;
            $tmp['isHidden']          = (java_values($v->isHidden()))?1:0;
            $tmp['DefaultValue']      = java_values($this->getTaskGetParameters()->getDefaultValue($v->getName()));

            if($v->getControlType() == '1') {
                $r = $this->getTaskGetParameters()->getSelectionList($v->getName());
                $tmp['Options'] = [];
                foreach($r as $v) {
                    $tmp['Options'][] = ['Label' => java_values($v->getLabel()), 'Value' => java_values($v->getValue())];
                }
            }

            $rt[] = $tmp;
        }

        return $rt;
    }

    public function getTaskGetParameters()
    {
        if (!$this->taskGetParameters) {
            $this->taskGetParameters = $this->_birtEngine->createGetParameterDefinitionTask($this->design);
        }
        return $this->taskGetParameters;
    }

    /**
     * Get list of options for a list parameter
     *
     * @param $name
     * @return mixed
     */
    public function getSelectionList($name)
    {
        return $this->getTaskGetParameters()->getSelectionList($name);
    }

    /**
     * Envia el Reporte renderizado al cliente
     *
     * @param string $fileName Nombre del archivo
     */
    public function sendStream($fileName = 'report')
    {
        switch ($this->_renderedFormat) {
            case "pdf":
                //header('Access-Control-Allow-Origin: *');
                header("Accept-Ranges: bytes");
                header("Content-type: application/pdf");
                header("Content-Disposition: inline; filename=\"{$fileName}.pdf\"");
                //header('Expires: ' . gmdate('D, d M Y H:i:s', gmmktime() - 3600) . ' GMT');
                break;
            case "html":
                header("Content-type: text/html; charset=utf-8");
                break;
            case "msword":
                header("Content-type: application/msword");
                header("Content-Disposition: inline; filename=\"{$fileName}.doc\"");
                break;
            case "xls":
                header("Content-type: application/vnd.ms-excel");
                header("Content-Disposition: inline; filename=\"{$fileName}.xls\"");
                break;
            default:
                throw new Exception("Rad_BirtEngine: formato '$this->_renderedFormat' desconocido");
        }
        echo $this->getStream();
    }
}