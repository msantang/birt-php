<?php namespace Msantang\BirtPhp;


use java;

/**
 * Msantang\Birt\Design
 *
 * @author Martin Alejandro Santangelo
 */
class Design
{
	protected $local = true;
	protected static $_preProcessors = array();

	protected $design;

	public function __construct($design)
	{
		$this->design = $design;
	}

	/**
	 * Open a local file into a bytearray
	 *
	 * @param  string $file local file to be loaded
	 * @throw Exception
	 */
	public static function fromLocalFile($file)
	{
		$xml = file_get_contents($file);

		if ($xml === false) throw new Exception("Cant open $file");

		$xml = static::runPreProcessors($xml);

		return new self(new java('java.io.ByteArrayInputStream', $xml));
	}

	/**
	 * Open file into a JavaBridge server file system
	 *
	 * @param  string $file local file to be loaded
	 */
	public function fromFile($file)
	{
		return new self($file);
	}

	protected function runPreProcessors($xml)
	{
		foreach (static::$_preProcessors as $prep) {
			if  ($prep instanceof Preprocesor) {
				$xml = $prep->run($xml);
			} else {
				$xml = $prep($xml);
			}
		}
		return $xml;
	}

	public static function addPreprocessor(Preprocesor $prep)
	{
		static::$_preProcessors[] = $prep;
	}

	public static function addCallbackPreprocessor(callable $prep)
	{
		static::$_preProcessors[] = $prep;
	}

	public function getDesign()
	{
		return $this->design;
	}
}